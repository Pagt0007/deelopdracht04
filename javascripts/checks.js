/**
 * @author Gert
 */
$(document).ready(function() {
	//resets the specific stated errors if there is a key up in the form related to it.
	$('#naam').keyup(function() {
		$('#naam-errors').html('');

		//#naam.keyup
	});
	$('#mail').keyup(function() {
		$('#mail-errors').html('');

		//#mail.keyup
	});
	$('#date').keyup(function() {
		$('#date-errors').html('');

		//#date.keyup
	});
	$('#phone').keyup(function() {
		$('#phone-errors').html('');

		//#date.keyup
	});
	//end of resets

	$('#inputs').validate({// specifies what form to validate

		rules : {// specifies what rules to use per input, defined by the name atribute
			naam : {
				required : true,
				minlength : 2,
				maxlength: 12,
				remote : {
					url : "../lib/functions.php",
					type : "POST",
					data : {//ajax call

						action : function() {
							value = "naam";

							return value;

						},
						input : function() {
							value = $("#naam").val();

							return value;
							//input
						}
						//data
					},
					success : function(response) {// if the return is true

						$('#naam-errors').html('Deze Naam is al in gebruik.');

						//success
					}
					//remote
				}
				//naam
			},
			mail : {
				required : true,
				email : true,
				remote : {
					url : "../lib/functions.php",
					type : "post",
					data : {//ajax call

						action : function() {
							value = "mail";
							return value;
							//action
						},
						input : function() {
							string = $("#mail").val();
							return string;

							//input
						}
						//data
					},
					success : function(response) {// if the return is true

						$('#mail-errors').html('Dit email adress in al in gebruik.');
						//success
					}
					//remote
				}

				//mail
			},
			pwd1 : {
				required : true,
				minlength : 8,
				maxlength : 20

				//pwd1
			},
			pwd2 : {
				equalTo : "#pwd1"

				//pwd2
			},

			date : {
				remote : {

					url : "../lib/functions.php",
					type : "post",
					data : {//ajax call

						action : function() {
							value = "date";
							return value;
							//action
						},
						input : function() {
							value = $("#date").val();

							return value;

							//input
						}
						//data
					},
					success : function(response) {// if the return is true

						$('#date-errors').html('U bent te jong.');
						//success
					}
					//remote
				}
				//date
			},

			phone : {
				required : true,
				remote : {

					url : "../lib/functions.php",
					type : "post",
					data : {//ajax call

						action : function() {

							value = "phone";
							return value;
							//action
						},
						input : function() {
							value = $("#phone").val();

							return value;

							//input
						}
						//data
					},
					success : function(response) {// if the return is true

						$('#phone-errors').html('Voer een geldig 06 nummer in.');
						//success
					}
					//remote
				}

				//phone
			}

			//rules
		},
		messages : {//specifies the error messages per attribute, and even per rule.
			naam : {
				required : "Voer een gebruikesnaam van minimaal 2 letters in.",
				minleght : "Voer een gebruikesnaam van minimaal 2 letters in.",
				//naam
			},
			mail : {
				required : "Vul aub uw email adres in.",
				email : "Voer aub een geldig email adres in."
				//mail
			},
			pwd1 : {
				required : "Voer aub een wachtwoord in.",
				minlength : "Uw wachtwoord moet minmaal 8 karakters lang zijn.",
				maxlength : "Uw wachtwoord mag maximaal 20 karakters lang zijn."
				//pwd1
			},
			pwd2 : "Wachtwoorden moeten gelijk aan elkaar zijn.",

			phone : {
				required : "Voer uw mobiele telfoon nummer in",
			}
			//messages
		}

		//#inputs.validate
	});

	//Extra stuff for validation that can't be done with the plugin

	$('#pwd1').focusout(function() {
		var passreq = new RegExp("[A-Z]");
		//hoofdletters zijn nodig

		if (!passreq.test($(this).val())) {
			$('#pwd1-errors').html('Uw wachtwoord moet minimaal 1 hoofdletter bevatten.');

		} else {
			$('#pwd1-errors').html('');
		}

		//#pwd1.focusout
	});

	//document.ready
});

