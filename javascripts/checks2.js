/**
 * @author Gert
 */
$(document).ready(function() {
	
	var naamch = ww1ch = ww2ch = datech = mailch = telch = false; 
 

	$('#naam').bind("keyup focusout", function() {

		var usernamePattern = new RegExp("[A-Za-z ]");
		var username2Pattern = new RegExp("[^A-Za-z ]");
		if (!usernamePattern.test($(this).val()) || username2Pattern.test($(this).val())) {
			$('#naam-errors').html('Uw gebruikesnaam mag alleen uit letters en spaties bestaan.');
			naamch = false;

		} else if ($(this).val().length < 2) {
			$('#naam-errors').html('Voer een gebruikesnaam van minimaal 2 letters in.');
			naamch = false;

		} else {
			$.ajax({
				url : '../lib/functions.php',
				type : 'post',
				dataType : "html",
				data : {
					action : function() {
						value = "naam";
						return value;
						//action
					},
					input : function() {
						value = $("#naam").val();
						return value;

						//input
					}
					//data
				},
				success : function(response) {// if the return is true
					if (response) {
						$('#naam-errors').html('Deze gebruikersaam is al in gebruik.');
						naamch = false;
					} else {
						$('#naam-errors').html('');
						naamch = true;
					}

					//success
				}
				//ajax
			});

			//this.length
		}

		//#naam.bind(keyup focusout
	});

	$('#pwd1').bind("keyup focusout", function() {

		var passreq = new RegExp("[A-Z]");
		var pwd1 = $(this).val();
		//hoofdletters zijn nodig

		if (pwd1.length == 0) {
			$('#pwd1-errors').html('Voer aub een wachtwoord in');
			ww1ch = false;

		} else if (!passreq.test(pwd1)) {
			$('#pwd1-errors').html('Uw wachtwoord moet minimaal 1 hoofdletter bevatten.');
			ww1ch = false;

		} else if (pwd1.length < 8) {
			$('#pwd1-errors').html('Uw wachtwoord moet uit minimaal 8 tekens bestaan.');
			ww1ch = false;

		} else if (pwd1.length > 20) {
			$('#pwd1-errors').html('Uw wachtwoord mag uit maxiaal 20 tekens bestaan.');
			ww1ch = false;
		} else {
			$('#pwd1-errors').html('');
			ww1ch = true;
		}

		//#pwd1.bind(keyup focusout
	});

	$('#pwd2').bind("keyup focusout", function() {
		var pwd1 = $('#pwd1').val();
		var pwd2 = $('#pwd2').val();
		if (pwd1 !== pwd2) {
			$('#pwd2-errors').html('Uw wachtwoorden moeten aan elkaar gelijk zijn.');
			ww2ch = false;
		} else {
			$('#pwd2-errors').html('');
			ww2ch = true;
		}

		//#pwd2.bind(keyup focusout
	});

	$('#date').bind("keyup focusout", function() {
		$.ajax({
				url : '../lib/functions.php',
				type : 'post',
				dataType : "html",
				data : {
					action : function() {
						value = "date";
						return value;
						//action
					},
					input : function() {
						value = $("#date").val();
						return value;

						//input
					}
					//data
				},
				success : function(response) {// if the return is true
					if (response) {
						$('#date-errors').html('U bent te jong om gebruik te maken van deze website');
						datech = false;
					} else {
						$('#date-errors').html('');
						datech = true;
					}

					//success
				}
				//ajax
			});
		

		//#date.bind(keyup focusout
	});

	$('#mail').bind("keyup focusout", function() {
		var mail = $('#mail').val();

		if (mail.length == 0) {
			$('#mail-errors').html('Vul aub uw email adres in.');
			mailch = false;
		} else if (!validateEmail(mail)) {
			$('#mail-errors').html('Voer aub een geldig email adres in.');
			mailch = false;
		} else {

			$.ajax({
				url : '../lib/functions.php',
				type : 'post',
				dataType : "html",
				data : {
					action : function() {
						value = "mail";
						return value;
						//action
					},
					input : function() {
						value = $("#mail").val();
						return value;

						//input
					}
					//data
				},
				success : function(response) {// if the return is true
					if (response) {
						$('#mail-errors').html('Dit email adress is al in gebruik');
						mailch = false;
					} else {
						$('#mail-errors').html('');
						mailch = true;
					}

					//success
				}
				//ajax
			});

		}

		function validateEmail(email) {
			var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
			return re.test(email);
		}

		//#mail.bind(keyup focusout
	});
	
	$('#phone').bind('keyup focusout', function(){
		var phone = $(this).val();
		var phonepat = new RegExp("[0-9]");
		var phonepat2 = new RegExp("[^0-9]"); 
		
		if(phone.length == 0){
			$('#phone-errors').html('Voer aub uw mobiele telefoon nummer in.');
			telch = false;
		}else if(phone.length !== 10){
			$('#phone-errors').html('Uw telefoon nummer moet uit 10 nummers bestaan.');
			telch = false;
		}else if(!phonepat.test($(this).val()) || phonepat2.test($(this).val())){
			$('#phone-errors').html('Uw telefoon nummer moet alleen uit nummers bestaan.');
			telch = false;
		}else{
			$('#phone-errors').html('');
			telch = true;
		}
				
		
		
		//#phone.bind(keyup focusout
	});
	
	$('#but').prop('disabled', true);
	$(document).mousemove(function(){
		//var naamch = ww1ch = ww2ch = datech = mailch = telch = false; 
		if(naamch && ww1ch && ww2ch && datech && mailch && telch){
			$('#but').prop('disabled', false);
		}else{
			$('#but').prop('disabled', true);
		}
		
		
		
		
	//document.mousemove	
	});

	//document.ready
});
