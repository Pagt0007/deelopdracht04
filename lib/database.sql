-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server versie:                5.6.26 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Versie:              9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Databasestructuur van formval wordt geschreven
DROP DATABASE IF EXISTS `formval`;
CREATE DATABASE IF NOT EXISTS `formval` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `formval`;


-- Structuur van  tabel formval.users wordt geschreven
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Naam` varchar(100) NOT NULL DEFAULT '0',
  `Wachtwoord` varchar(255) NOT NULL DEFAULT '0',
  `Geboortedatum` date NOT NULL DEFAULT '0000-00-00',
  `Email` varchar(50) NOT NULL DEFAULT '0',
  `Telefoonnummer` varchar(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel formval.users: ~9 rows (ongeveer)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`ID`, `Naam`, `Wachtwoord`, `Geboortedatum`, `Email`, `Telefoonnummer`) VALUES
	(12, 'Gert de Pagter', '$2y$10$O3NkbGZrajtzZGtqYXNmauJ7PaNJK4YzOHyn4tCJwlnZQJzbQ9OVS', '1997-01-01', 'jan@hz.nl', '0612345678'),
	(13, 'Hen de Jager', '$2y$10$O3NkbGZrajtzZGtqYXNmauG1n755KiEbvJAvEBJ8DSNdTKXJiSZlq', '1992-06-09', 'Jager_hen@hz.nl', '0612345687'),
	(17, 'Jan de vries', '$2y$10$O3NkbGZrajtzZGtqYXNmauJ7PaNJK4YzOHyn4tCJwlnZQJzbQ9OVS', '2001-02-04', 'jan@jan.jan', '0612345678'),
	(18, 'asdfasdfasdf sadg', '$2y$10$O3NkbGZrajtzZGtqYXNmau/la598C5AYI6eYpcaHUoqeJjJN0CFYO', '1993-12-12', 'jan@piet.co', '0616548465');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
