<?php
	// Open the database
	$url = 'localhost';
	$userid = 'root';
	$password = '';
	$database = 'formval';
	$mysqli = new mysqli($url, $userid, $password, $database);
	if ($mysqli-> connect_errno) {
		die("Failed to connect to MySQL: (" . $mysqli -> connect_errno . ") " . $mysqli -> connect_error);
	}
	
if(isset($_POST['action'])) {
    $action = $_POST['action'];
	$input = $_POST['input'];
	
    if($action == 'naam') {
    	
    	if(uniquename($input)){
    		echo FALSE;
			
    	}else{
    		
			echo true;
    	}
	// $action = naam	
    }
	else if($action == 'mail'){
		
		if(mailexists($input)){
			echo false;
		}else{
			echo true;
		}
	// $action == mail	
	}
	else if($action =='date'){
		
		if(checkbrithday($input)){
			echo false;
		}else{
			echo true;
		}
		
	// $action == date
	}
	if ($action == 'phone'){
		
		if(checkphone($input)){
			echo false;
		}else{
			echo true;
		}
	
	// $action == phone
	}

//isset Post action		
}





function test_input($data) { 
	if(isset($data)){ 
	$data = trim($data); //removes extra spaces
	$data = stripslashes($data); //removes backslahses
	$data = htmlspecialchars($data); //removes special character that have no place there
	return $data;
	}else{
		return false;
	}	
}

function uniquename($naam) {
	global $mysqli; 
	$result = $mysqli->query( "SELECT * FROM users WHERE Naam = '" . $naam . "'");
	if($result->num_rows == 0){
		return true;
	}
	else {
		return false;
	} } 
function onlytext($input){//checks to see if the $input only contains numbers and spaces;
	if(preg_match('/^[a-zA-Z\s]*$/i', $input)){
		return true;
	}else{
		return false;
	}
}

function check_passwords($pw1, $pw2){ // checks if passwords are the same, and if they are more then 8 and less then 20 chars
	if($pw1 == $pw2){
		return true;
	}else{
		return false;
	}
	
}
function checklength($check, $min,$max){// checks if the lenght of a string is between min and max
	if(strlen($check) > $min && strlen($check) < $max){
		return true;
	}else{
		return false;
	}
	
}

function hashpwd($pwd){//hashes the password for security
	$options = ['salt' => ';sdlfkj;sdkjasfkjasd;kfaj;fqasj;'];
	$hash = password_hash($pwd, PASSWORD_DEFAULT, $options);
	return $hash;
}

function checkbrithday($birthday){ // checks if the input day is 13 or more years away from the current day.
	$now = new DateTime();
	$birthday = new DateTime($birthday);
	$interval = date_diff($now, $birthday);
	if($interval->y >= 13 && $birthday < $now){
		return true;
	} else {
		return false;
	}
}

function checkmail($mail){ // checks if the email is a valid email and if the email doesn't already exist
	global $mysqli;
	$email =  $mysqli->query("SELECT * FROM users WHERE Email = '" . $mail . "'");
	if (filter_var($mail, FILTER_VALIDATE_EMAIL) && $email->num_rows == 0) {
    	return true;
	}else{
		return false;
	}
}

function mailexists($mail){
global $mysqli;
	$email =  $mysqli->query("SELECT * FROM users WHERE Email = '" . $mail . "'");
	if ($email->num_rows == 0) {
    	return true;
	}else{
		return false;
	}
}

function checkphone($phone){ //all numbers need to start with 06, and can have 8 more characters after that
	if (preg_match( '/^06(-)?\d{8}$/', $phone )) {
		return true;
	} else {
		return false;
	}
}



?>