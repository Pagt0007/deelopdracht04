<?php
include_once '../lib/functions.php';
//all custom php functions are stored here
?>
<!DOCTYPE html>
<html>
	<head>
		<title>POC Form Validation</title>
		<!-- Main CSS -->
		<link type="text/css" href="../stylesheets/stylesheet.css" rel="stylesheet" />

	</head>
	<body>
		<h1>Proof of concept form validation</h1>
		<h2>Met de plugin</h2>

		<?php
		//resets the variables if they were entered before
		$naam = $pwd1 = $pwd2 = $date = $email = $telnr = "";
		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			//trims all the data of things like extra spaces or backslahses
			$naam = test_input($_POST['naam']);
			$pwd1 = test_input($_POST['pwd1']);
			$pwd2 = test_input($_POST['pwd2']);
			$date = test_input($_POST['date']);
			$email = test_input($_POST['mail']);
			$telnr = test_input($_POST['phone']);
			if (uniquename($naam) && check_passwords($pwd1, $pwd2) && checklength($pwd1, 8, 20) && (onlytext($naam)) && checkbrithday($date) && checkmail($email) && checkphone($telnr)) {
				$hash = hashpwd($pwd1);
				$sql = "INSERT INTO users VALUES ( 0, '" . $naam . "', '" . $hash . "', '" . $date . "', '" . $email . "', '" . $telnr . "')";
				$mysqli -> query($sql);
				header("Location: ../pages/success.php");
				exit ;

			}

		}
		?>

		<form id="inputs" method="post">
			<p>
				Gebruikers naam
				<input id="naam" type="text" name="naam" placeholder="Alleen letters en spaties" required>
				<label id="naam-errors" class="errors"></label>
			</p>
			<p>
				Wachtwoord
				<input id="pwd1" type="password" name="pwd1" placeholder="Uw wachtwoord" maxlength="20" required>
				<label id="pwd1-errors" class="errors"></label>
			</p>
			<p>
				Herhaal uw wachtwoord
				<input id="pwd2" type="password" name="pwd2" placeholder="Herhaal uw wachtwoord" maxlength="20" required>
				
			</p>
			<p>
				Geboorte Datum
				<input id="date" type="date" name="date"  required>
				<label id="date-errors" class="errors"></label>
			</p>
			<p>
				Email adres
				<input id="mail" type="email" name="mail" required>
				<label id="mail-errors" class="errors"></label>
			</p>
			<p>
				Mobiel telefoon nummer
				<input id="phone" type="tel" name="phone" maxlength="10" placeholder="Aleen geldige 06 nummers" required>
				<label id="phone-errors" class="errors"></label>
			</p>
			<input type="submit" name="submit" value="Invoeren">

		</form>

	</body>
	<?php 		//resets the variables again
		$naam = $pwd1 = $pwd2 = $date = $email = $telnr = "";
	?>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/jquery.validate.js"></script>
	<script src="../javascripts/checks.js"></script>
</html>